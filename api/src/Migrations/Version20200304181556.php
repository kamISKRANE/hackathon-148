<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200304181556 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE document_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE mission_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE domaine_de_competence_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE projet_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE evenement_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE corps_de_metier_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE greeting_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE user_account_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE prise_rdv_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE competence_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE document (id INT NOT NULL, libelle VARCHAR(250) NOT NULL, note_difficuulte INT DEFAULT NULL, url VARCHAR(250) NOT NULL, date_creation DATE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE mission (id INT NOT NULL, libelle VARCHAR(250) NOT NULL, description VARCHAR(250) DEFAULT NULL, date_debut DATE DEFAULT NULL, date_fin DATE DEFAULT NULL, prix INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE domaine_de_competence (id INT NOT NULL, id_corps_de_metier_id INT DEFAULT NULL, libelle VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_DE64F350F3AA6A41 ON domaine_de_competence (id_corps_de_metier_id)');
        $this->addSql('CREATE TABLE projet (id INT NOT NULL, mission_id INT DEFAULT NULL, libelle VARCHAR(250) NOT NULL, nom_du_client VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_50159CA9BE6CAE90 ON projet (mission_id)');
        $this->addSql('CREATE TABLE evenement (id INT NOT NULL, titre VARCHAR(250) NOT NULL, description VARCHAR(250) DEFAULT NULL, lieu VARCHAR(250) NOT NULL, date_evenement DATE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE mission_competence (id_competence_id INT NOT NULL, id_mission_id INT NOT NULL, description VARCHAR(250) DEFAULT NULL, note INT DEFAULT NULL, PRIMARY KEY(id_competence_id, id_mission_id))');
        $this->addSql('CREATE INDEX IDX_D6D445E4AB5ECCCE ON mission_competence (id_competence_id)');
        $this->addSql('CREATE INDEX IDX_D6D445E41BE62E47 ON mission_competence (id_mission_id)');
        $this->addSql('CREATE TABLE corps_de_metier (id INT NOT NULL, libelle VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE greeting (id INT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_account (id INT NOT NULL, nom VARCHAR(250) NOT NULL, prenom VARCHAR(250) NOT NULL, email VARCHAR(250) NOT NULL, password VARCHAR(250) NOT NULL, role VARCHAR(250) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_account_competence (user_account_id INT NOT NULL, competence_id INT NOT NULL, PRIMARY KEY(user_account_id, competence_id))');
        $this->addSql('CREATE INDEX IDX_C40B7C203C0C9956 ON user_account_competence (user_account_id)');
        $this->addSql('CREATE INDEX IDX_C40B7C2015761DAB ON user_account_competence (competence_id)');
        $this->addSql('CREATE TABLE user_account_evenement (user_account_id INT NOT NULL, evenement_id INT NOT NULL, PRIMARY KEY(user_account_id, evenement_id))');
        $this->addSql('CREATE INDEX IDX_D1430B6E3C0C9956 ON user_account_evenement (user_account_id)');
        $this->addSql('CREATE INDEX IDX_D1430B6EFD02F13 ON user_account_evenement (evenement_id)');
        $this->addSql('CREATE TABLE prise_rdv (id INT NOT NULL, libelle VARCHAR(250) DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE competence (id INT NOT NULL, libelle VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE domaine_de_competence ADD CONSTRAINT FK_DE64F350F3AA6A41 FOREIGN KEY (id_corps_de_metier_id) REFERENCES corps_de_metier (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE projet ADD CONSTRAINT FK_50159CA9BE6CAE90 FOREIGN KEY (mission_id) REFERENCES mission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission_competence ADD CONSTRAINT FK_D6D445E4AB5ECCCE FOREIGN KEY (id_competence_id) REFERENCES competence (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE mission_competence ADD CONSTRAINT FK_D6D445E41BE62E47 FOREIGN KEY (id_mission_id) REFERENCES mission (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account_competence ADD CONSTRAINT FK_C40B7C203C0C9956 FOREIGN KEY (user_account_id) REFERENCES user_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account_competence ADD CONSTRAINT FK_C40B7C2015761DAB FOREIGN KEY (competence_id) REFERENCES competence (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account_evenement ADD CONSTRAINT FK_D1430B6E3C0C9956 FOREIGN KEY (user_account_id) REFERENCES user_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_account_evenement ADD CONSTRAINT FK_D1430B6EFD02F13 FOREIGN KEY (evenement_id) REFERENCES evenement (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agent_competence DROP CONSTRAINT fk_e592b9703414710b');
        $this->addSql('DROP SEQUENCE operateur_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE agent_id_seq CASCADE');
        $this->addSql('CREATE TABLE user_competence (user_id INT NOT NULL, competence_id INT NOT NULL, PRIMARY KEY(user_id, competence_id))');
        $this->addSql('CREATE INDEX IDX_33B3AE93A76ED395 ON user_competence (user_id)');
        $this->addSql('CREATE INDEX IDX_33B3AE9315761DAB ON user_competence (competence_id)');
        $this->addSql('CREATE TABLE user_prise_rdv (user_id INT NOT NULL, prise_rdv_id INT NOT NULL, PRIMARY KEY(user_id, prise_rdv_id))');
        $this->addSql('CREATE INDEX IDX_46999B76A76ED395 ON user_prise_rdv (user_id)');
        $this->addSql('CREATE INDEX IDX_46999B76AD97D5A7 ON user_prise_rdv (prise_rdv_id)');
        $this->addSql('ALTER TABLE user_competence ADD CONSTRAINT FK_33B3AE93A76ED395 FOREIGN KEY (user_id) REFERENCES user_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_competence ADD CONSTRAINT FK_33B3AE9315761DAB FOREIGN KEY (competence_id) REFERENCES competence (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_prise_rdv ADD CONSTRAINT FK_46999B76A76ED395 FOREIGN KEY (user_id) REFERENCES user_account (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_prise_rdv ADD CONSTRAINT FK_46999B76AD97D5A7 FOREIGN KEY (prise_rdv_id) REFERENCES prise_rdv (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE operateur');
        $this->addSql('DROP TABLE agent');
        $this->addSql('DROP TABLE agent_competence');
        $this->addSql('ALTER TABLE projet ADD user_referent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE projet ADD CONSTRAINT FK_50159CA99CFFB7A0 FOREIGN KEY (user_referent_id) REFERENCES user_account (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_50159CA99CFFB7A0 ON projet (user_referent_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE projet DROP CONSTRAINT FK_50159CA9BE6CAE90');
        $this->addSql('ALTER TABLE mission_competence DROP CONSTRAINT FK_D6D445E41BE62E47');
        $this->addSql('ALTER TABLE user_account_evenement DROP CONSTRAINT FK_D1430B6EFD02F13');
        $this->addSql('ALTER TABLE domaine_de_competence DROP CONSTRAINT FK_DE64F350F3AA6A41');
        $this->addSql('ALTER TABLE user_account_competence DROP CONSTRAINT FK_C40B7C203C0C9956');
        $this->addSql('ALTER TABLE user_account_evenement DROP CONSTRAINT FK_D1430B6E3C0C9956');
        $this->addSql('ALTER TABLE mission_competence DROP CONSTRAINT FK_D6D445E4AB5ECCCE');
        $this->addSql('ALTER TABLE user_account_competence DROP CONSTRAINT FK_C40B7C2015761DAB');
        $this->addSql('DROP SEQUENCE document_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE mission_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE domaine_de_competence_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE projet_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE evenement_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE corps_de_metier_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE greeting_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE user_account_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE prise_rdv_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE competence_id_seq CASCADE');
        $this->addSql('DROP TABLE document');
        $this->addSql('DROP TABLE mission');
        $this->addSql('DROP TABLE domaine_de_competence');
        $this->addSql('DROP TABLE projet');
        $this->addSql('DROP TABLE evenement');
        $this->addSql('DROP TABLE mission_competence');
        $this->addSql('DROP TABLE corps_de_metier');
        $this->addSql('DROP TABLE greeting');
        $this->addSql('DROP TABLE user_account');
        $this->addSql('DROP TABLE user_account_competence');
        $this->addSql('DROP TABLE user_account_evenement');
        $this->addSql('DROP TABLE prise_rdv');
        $this->addSql('DROP TABLE competence');
        $this->addSql('CREATE SEQUENCE operateur_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE agent_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE operateur (id INT NOT NULL, projet_id INT DEFAULT NULL, nom VARCHAR(250) NOT NULL, prenom VARCHAR(250) NOT NULL, email VARCHAR(250) NOT NULL, password VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_b4b7f99dc18272 ON operateur (projet_id)');
        $this->addSql('CREATE TABLE agent (id INT NOT NULL, nom VARCHAR(250) NOT NULL, prenom VARCHAR(250) NOT NULL, email VARCHAR(250) NOT NULL, password VARCHAR(250) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE agent_competence (agent_id INT NOT NULL, competence_id INT NOT NULL, PRIMARY KEY(agent_id, competence_id))');
        $this->addSql('CREATE INDEX idx_e592b97015761dab ON agent_competence (competence_id)');
        $this->addSql('CREATE INDEX idx_e592b9703414710b ON agent_competence (agent_id)');
        $this->addSql('ALTER TABLE operateur ADD CONSTRAINT fk_b4b7f99dc18272 FOREIGN KEY (projet_id) REFERENCES projet (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agent_competence ADD CONSTRAINT fk_e592b9703414710b FOREIGN KEY (agent_id) REFERENCES agent (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE agent_competence ADD CONSTRAINT fk_e592b97015761dab FOREIGN KEY (competence_id) REFERENCES competence (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('DROP TABLE user_competence');
        $this->addSql('DROP TABLE user_prise_rdv');
        $this->addSql('ALTER TABLE projet DROP CONSTRAINT FK_50159CA99CFFB7A0');
        $this->addSql('DROP INDEX IDX_50159CA99CFFB7A0');
        $this->addSql('ALTER TABLE projet DROP user_referent_id');
    }
}
