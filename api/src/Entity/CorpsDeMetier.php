<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CorpsDeMetierRepository")
 */
class CorpsDeMetier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DomaineDeCompetence", mappedBy="id_corps_de_metier")
     */
    private $domaineDeCompetences;

    public function __construct()
    {
        $this->domaineDeCompetences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|DomaineDeCompetence[]
     */
    public function getDomaineDeCompetences(): Collection
    {
        return $this->domaineDeCompetences;
    }

    public function addDomaineDeCompetence(DomaineDeCompetence $domaineDeCompetence): self
    {
        if (!$this->domaineDeCompetences->contains($domaineDeCompetence)) {
            $this->domaineDeCompetences[] = $domaineDeCompetence;
            $domaineDeCompetence->setIdCorpsDeMetier($this);
        }

        return $this;
    }

    public function removeDomaineDeCompetence(DomaineDeCompetence $domaineDeCompetence): self
    {
        if ($this->domaineDeCompetences->contains($domaineDeCompetence)) {
            $this->domaineDeCompetences->removeElement($domaineDeCompetence);
            // set the owning side to null (unless already changed)
            if ($domaineDeCompetence->getIdCorpsDeMetier() === $this) {
                $domaineDeCompetence->setIdCorpsDeMetier(null);
            }
        }

        return $this;
    }
}
