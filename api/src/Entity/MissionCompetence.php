<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\MissionCompetenceRepository")
 */
class MissionCompetence
{

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Competence", inversedBy="missionCompetences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_competence;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\Mission", inversedBy="missionCompetences")
     * @ORM\JoinColumn(nullable=false)
     */
    private $id_mission;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $note;

    public function getnote(): ?int
    {
        return $this->note;
    }

    public function setnote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIdCompetence(): ?Competence
    {
        return $this->id_competence;
    }

    public function setIdCompetence(?Competence $id_competence): self
    {
        $this->id_competence = $id_competence;

        return $this;
    }

    public function getIdMission(): ?Mission
    {
        return $this->id_mission;
    }

    public function setIdMission(?Mission $id_mission): self
    {
        $this->id_mission = $id_mission;

        return $this;
    }

}
