<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\MissionRepository")
 */
class Mission
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=250, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_debut;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $date_fin;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="mission")
     */
    private $id_projet;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $prix;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MissionCompetence", mappedBy="id_mission", orphanRemoval=true)
     */
    private $missionCompetences;


    public function __construct()
    {
        $this->id_projet = new ArrayCollection();
        $this->missionCompetences = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDateDebut(): ?\DateTimeInterface
    {
        return $this->date_debut;
    }

    public function setDateDebut(?\DateTimeInterface $date_debut): self
    {
        $this->date_debut = $date_debut;

        return $this;
    }

    public function getDateFin(): ?\DateTimeInterface
    {
        return $this->date_fin;
    }

    public function setDateFin(?\DateTimeInterface $date_fin): self
    {
        $this->date_fin = $date_fin;

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getIdProjet(): Collection
    {
        return $this->id_projet;
    }

    public function addIdProjet(Projet $idProjet): self
    {
        if (!$this->id_projet->contains($idProjet)) {
            $this->id_projet[] = $idProjet;
            $idProjet->setMission($this);
        }

        return $this;
    }

    public function removeIdProjet(Projet $idProjet): self
    {
        if ($this->id_projet->contains($idProjet)) {
            $this->id_projet->removeElement($idProjet);
            // set the owning side to null (unless already changed)
            if ($idProjet->getMission() === $this) {
                $idProjet->setMission(null);
            }
        }

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(?int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * @return Collection|MissionCompetence[]
     */
    public function getMissionCompetences(): Collection
    {
        return $this->missionCompetences;
    }

    public function addMissionCompetence(MissionCompetence $missionCompetence): self
    {
        if (!$this->missionCompetences->contains($missionCompetence)) {
            $this->missionCompetences[] = $missionCompetence;
            $missionCompetence->setIdMission($this);
        }

        return $this;
    }

    public function removeMissionCompetence(MissionCompetence $missionCompetence): self
    {
        if ($this->missionCompetences->contains($missionCompetence)) {
            $this->missionCompetences->removeElement($missionCompetence);
            // set the owning side to null (unless already changed)
            if ($missionCompetence->getIdMission() === $this) {
                $missionCompetence->setIdMission(null);
            }
        }

        return $this;
    }

}
