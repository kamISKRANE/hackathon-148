<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $libelle;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $nom_du_client;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Mission", inversedBy="id_projet")
     */
    private $mission;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="projet_referent")
     */
    private $user_referent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getNomDuClient(): ?string
    {
        return $this->nom_du_client;
    }

    public function setNomDuClient(string $nom_du_client): self
    {
        $this->nom_du_client = $nom_du_client;

        return $this;
    }

    public function getMission(): ?Mission
    {
        return $this->mission;
    }

    public function setMission(?Mission $mission): self
    {
        $this->mission = $mission;

        return $this;
    }

    public function getUserReferent(): ?User
    {
        return $this->user_referent;
    }

    public function setUserReferent(?User $user_referent): self
    {
        $this->user_referent = $user_referent;

        return $this;
    }

}
