<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_account")
 * @UniqueEntity(fields={"email"}, message="There is already an account with this email")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Competence", inversedBy="users")
     */
    private $competence;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Projet", mappedBy="user_referent")
     */
    private $projet_referent;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PriseRdv", inversedBy="users")
     */
    private $prise_rdv;


    public function __construct()
    {
        $this->competence = new ArrayCollection();
        $this->projet_referent = new ArrayCollection();
        $this->prise_rdv = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return Collection|Competence[]
     */
    public function getCompetence(): Collection
    {
        return $this->competence;
    }

    public function addCompetence(Competence $competence): self
    {
        if (!$this->competence->contains($competence)) {
            $this->competence[] = $competence;
        }

        return $this;
    }

    public function removeCompetence(Competence $competence): self
    {
        if ($this->competence->contains($competence)) {
            $this->competence->removeElement($competence);
        }

        return $this;
    }

    /**
     * @return Collection|Projet[]
     */
    public function getProjetReferent(): Collection
    {
        return $this->projet_referent;
    }

    public function addProjetReferent(Projet $projetReferent): self
    {
        if (!$this->projet_referent->contains($projetReferent)) {
            $this->projet_referent[] = $projetReferent;
            $projetReferent->setUserReferent($this);
        }

        return $this;
    }

    public function removeProjetReferent(Projet $projetReferent): self
    {
        if ($this->projet_referent->contains($projetReferent)) {
            $this->projet_referent->removeElement($projetReferent);
            // set the owning side to null (unless already changed)
            if ($projetReferent->getUserReferent() === $this) {
                $projetReferent->setUserReferent(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PriseRdv[]
     */
    public function getPriseRdv(): Collection
    {
        return $this->prise_rdv;
    }

    public function addPriseRdv(PriseRdv $priseRdv): self
    {
        if (!$this->prise_rdv->contains($priseRdv)) {
            $this->prise_rdv[] = $priseRdv;
        }

        return $this;
    }

    public function removePriseRdv(PriseRdv $priseRdv): self
    {
        if ($this->prise_rdv->contains($priseRdv)) {
            $this->prise_rdv->removeElement($priseRdv);
        }

        return $this;
    }

}
