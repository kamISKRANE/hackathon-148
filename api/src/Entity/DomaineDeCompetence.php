<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\DomaineDeCompetenceRepository")
 */
class DomaineDeCompetence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $libelle;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CorpsDeMetier", inversedBy="domaineDeCompetences")
     */
    private $id_corps_de_metier;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getIdCorpsDeMetier(): ?CorpsDeMetier
    {
        return $this->id_corps_de_metier;
    }

    public function setIdCorpsDeMetier(?CorpsDeMetier $id_corps_de_metier): self
    {
        $this->id_corps_de_metier = $id_corps_de_metier;

        return $this;
    }
}
