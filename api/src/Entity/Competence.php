<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass="App\Repository\CompetenceRepository")
 */
class Competence
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $libelle;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MissionCompetence", mappedBy="id_competence", orphanRemoval=true)
     */
    private $missionCompetences;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="competence")
     */
    private $users;

    public function __construct()
    {
        $this->missionCompetences = new ArrayCollection();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection|MissionCompetence[]
     */
    public function getMissionCompetences(): Collection
    {
        return $this->missionCompetences;
    }

    public function addMissionCompetence(MissionCompetence $missionCompetence): self
    {
        if (!$this->missionCompetences->contains($missionCompetence)) {
            $this->missionCompetences[] = $missionCompetence;
            $missionCompetence->setIdCompetence($this);
        }

        return $this;
    }

    public function removeMissionCompetence(MissionCompetence $missionCompetence): self
    {
        if ($this->missionCompetences->contains($missionCompetence)) {
            $this->missionCompetences->removeElement($missionCompetence);
            // set the owning side to null (unless already changed)
            if ($missionCompetence->getIdCompetence() === $this) {
                $missionCompetence->setIdCompetence(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->addCompetence($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
            $user->removeCompetence($this);
        }

        return $this;
    }

}
