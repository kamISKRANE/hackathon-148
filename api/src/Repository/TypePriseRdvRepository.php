<?php

namespace App\Repository;

use App\Entity\TypePriseRdv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypePriseRdv|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypePriseRdv|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypePriseRdv[]    findAll()
 * @method TypePriseRdv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypePriseRdvRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypePriseRdv::class);
    }

    // /**
    //  * @return TypePriseRdv[] Returns an array of TypePriseRdv objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypePriseRdv
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
