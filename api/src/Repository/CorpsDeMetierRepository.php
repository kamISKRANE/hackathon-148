<?php

namespace App\Repository;

use App\Entity\CorpsDeMetier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CorpsDeMetier|null find($id, $lockMode = null, $lockVersion = null)
 * @method CorpsDeMetier|null findOneBy(array $criteria, array $orderBy = null)
 * @method CorpsDeMetier[]    findAll()
 * @method CorpsDeMetier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CorpsDeMetierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CorpsDeMetier::class);
    }

    // /**
    //  * @return CorpsDeMetier[] Returns an array of CorpsDeMetier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CorpsDeMetier
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
