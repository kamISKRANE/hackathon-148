<?php

namespace App\Repository;

use App\Entity\DomaineDeCompetence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DomaineDeCompetence|null find($id, $lockMode = null, $lockVersion = null)
 * @method DomaineDeCompetence|null findOneBy(array $criteria, array $orderBy = null)
 * @method DomaineDeCompetence[]    findAll()
 * @method DomaineDeCompetence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DomaineDeCompetenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DomaineDeCompetence::class);
    }

    // /**
    //  * @return DomaineDeCompetence[] Returns an array of DomaineDeCompetence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DomaineDeCompetence
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
