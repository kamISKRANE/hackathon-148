<?php

namespace App\Repository;

use App\Entity\MissionCompetence;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method MissionCompetence|null find($id, $lockMode = null, $lockVersion = null)
 * @method MissionCompetence|null findOneBy(array $criteria, array $orderBy = null)
 * @method MissionCompetence[]    findAll()
 * @method MissionCompetence[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MissionCompetenceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MissionCompetence::class);
    }

    // /**
    //  * @return MissionCompetence[] Returns an array of MissionCompetence objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MissionCompetence
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
