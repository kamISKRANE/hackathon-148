import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './helpers/helpers';
import App from './App';
import { Provider } from 'react-redux';
import * as serviceWorker from './serviceWorker';
import { initializeStore, initializeSagas } from './configs/store';
// init store
const store = initializeStore();
// init sagas
initializeSagas();
// render application
ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>
    , document.getElementById('root'));
// service workers
serviceWorker.register();
