import { all } from 'redux-saga/effects';
import authSaga from '../components/Auth/saga';
import competencesSaga from '../components/Competences/saga';

export function* rootSagas() {
  yield all ([
    authSaga(),
    competencesSaga(),
  ]);
}
