import React from 'react';
import { Router, Switch, Route, Redirect } from 'react-router-dom';
import history from './configs/history';
import NotFound from "./pages/NotFound";
import AuthRoute from "./components/Auth/AuthRoute";
import routes from './routes';
import './main.css';

const generateRoutes = (routes) => routes.map((route, key) => (
  (route.private === false)
    ? <Route key={key} {...route} />
    : <AuthRoute
        key={key}
        {...route} 
      />
));

class  App extends React.Component {
  
  render() {
    return (
        <Router history={history}>
          <>
            <Switch>
              
              {/* Routes */}
              {generateRoutes(routes)}
              
              {/* Not Found */}
              <Route exact path={"/not-found"} component={NotFound} />
              <Route render={() => <Redirect to={"/not-found"} />} />

            </Switch>
          </>
        </Router>
    );
  }
}

export default App;
