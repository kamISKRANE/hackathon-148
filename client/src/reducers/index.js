import { combineReducers } from 'redux';
/* Reducers import */
import greetingsReducer from '../components/Greetings/reducer';
import competencesReducer from '../components/Competences/reducer';

const createReducer = asyncReducers =>
  combineReducers({
    greetings: greetingsReducer,
    competences: competencesReducer,
});

export default createReducer;
