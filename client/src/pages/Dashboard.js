import React from 'react'
import Header from '../components/Header/Header';
import MargedTopContainer from '../components/Containers/MargedTopContainer';
import {Container, Row, Col} from 'react-bootstrap';
import DashboardHeader from '../components/Header/DashboardHeader';
import OpenedSpaces from '../components/Spaces/OpenedSpaces';
import ComingUpSpaces from '../components/Spaces/ComingUpSpaces';
import OverSpaces from '../components/Spaces/OverSpaces';
import Fade from '../components/Navigation/Fade';
import history from '../configs/history';

class Dashboard extends React.Component {
    render () {
        const { match } = this.props;
        const { dashboard_status } = match.params;

        if (dashboard_status && !([
            'salons-a-venir',
            'salons-termines'
        ].findIndex(uri => uri === dashboard_status) >= 0)) history.replace('/404.html');
        
        return (
            <React.Fragment>
                <Header />
                <Container style={{padding: 0}}>
                    <DashboardHeader  dashboard_status={dashboard_status}/>
                </Container>
                <MargedTopContainer>
                    <Container>
                        <Row className={'justify-content-center'}>
                            <Col lg={10}>
                                {
                                    (dashboard_status === undefined) &&
                                        <Fade>
                                            <OpenedSpaces />
                                        </Fade>
                                }
                                {
                                    (dashboard_status === 'salons-a-venir') &&
                                        <Fade>
                                            <ComingUpSpaces />
                                        </Fade>     
                                }
                                {
                                    (dashboard_status === 'salons-termines') &&
                                        <Fade>
                                            <OverSpaces />
                                        </Fade>
                                }
                            </Col>
                        </Row>
                    </Container>
                </MargedTopContainer>
            </React.Fragment>
        );
    }
}

export default Dashboard;