import React from 'react';
import AdminMenu from '../../components/Menus/AdminMenu';

class Dashboard extends React.Component {
    render() { 
        return (
            <div>
                <AdminMenu />
                ADMIN Dashboard
            </div>
        );
    }
}
 
export default Dashboard;