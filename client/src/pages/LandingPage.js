import React, { Component, Fragment } from 'react';
import Container from '../components/Landing/Container';
import Header from '../components/Landing/Header';
import TitleBrand from '../components/Landing/TitleBrand';
import CTA from '../components/Landing/CTA';
import Explanation from '../components/Landing/Explanation';    


class LandingPage extends Component {
    render() { 
        return (
            <Fragment>
                
                <Container>
                    
                    <Header />
                    <TitleBrand />
                    
                    <CTA id='top'/>
                    <CTA id='left'/>
                    <CTA id='right'/>                

                </Container>

            </Fragment>


                

         );
    }
}
 
export default LandingPage;