import React from 'react';
import AgentMenu from '../../components/Menus/AgentMenu';
import GreetingsList from '../../components/Greetings/GreetingsList';

class Dashboard extends React.Component {
    render() { 
        return (
            <div>
                <AgentMenu />
                Im a Dashboard
                <GreetingsList />
            </div>
        );
    }
}
 
export default Dashboard;