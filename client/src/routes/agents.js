import RegisterForm from '../components/Auth/RegisterForm';
import Dashboard from '../pages/Agents/Dashboard';
import DatabaseKnowledge from '../pages/Agents/DatabaseKnowledge';
import Events from '../pages/Agents/Events';
import Candidates from '../pages/Agents/Candidates';
import AuthForm from '../components/Auth/AuthForm';

export default [
    {
        path: '/inscription',
        component: RegisterForm,
        private: false,
        exact: true,
    },
    {
        path: '/login',
        component: AuthForm,
        private: false,
        exact: true,
    },
    {
        path: '/dashboard',
        component: Dashboard,
        private: false,
        exact: true,
    },
    {
        path: '/database-knowledge',
        component: DatabaseKnowledge,
        private: false,
        exact: true,
    },
    {
        path: '/events',
        component: Events,
        private: false,
        exact: true,
    },
    {
        path: '/candidates',
        component: Candidates,
        private: false,
        exact: true,
    },
];