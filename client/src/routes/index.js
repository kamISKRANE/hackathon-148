import React from 'react'
import { Route } from 'react-router-dom';
import AuthRoute from '../components/Auth/AuthRoute';
import main from './main';
import auth from './auth';
import agents from './agents';
import admin from './admin';

const combineRoutes = routes => {
  return routes.reduce((accumulator, route) => [...accumulator, ...route]);
};

export const generateRoutes = (routes) => {
  return routes.map(route => (
    (!route.private || route.private === true)
    ? <Route {...route} />
    : <AuthRoute 
        {...route} 
      />
    ));
  }
  
  const routes = combineRoutes([
    auth,
    main,
    agents,
    admin,
  ]);

export const customRouteGenerator = (_customGenerator, _routes) => _customGenerator(_routes);

export default routes;