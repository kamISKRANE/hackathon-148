import AuthForm from "../components/Auth/AuthForm";

export default [
    {
        path: '/login',
        component: AuthForm,
        exact: true,
        private: false,
    },
];