import Dashboard from '../pages/Admin/Dashboard';
import DatabaseKnowledge from '../pages/Admin/DatabaseKnowledge';
import Events from '../pages/Admin/Events';
import Candidates from '../pages/Admin/Candidates';
import CorpsDeMetiers from '../components/Competences/CorpsDeMetiers';

export default [
    {
        path: '/admin/dashboard',
        component: Dashboard,
        private: false,
        exact: true,
    },
    {
        path: '/admin/database-knowledge',
        component: DatabaseKnowledge,
        private: false,
        exact: true,
    },
    {
        path: '/admin/events',
        component: Events,
        private: false,
        exact: true,
    },
    {
        path: '/admin/candidates',
        component: Candidates,
        private: false,
        exact: true,
    },
    {
        path: '/admin/competences',
        component: CorpsDeMetiers,
        private: false,
        exact: true,
    },
];