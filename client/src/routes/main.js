import React, { Component } from 'react';
import LandingPage from '../pages/LandingPage';

export default [
    {
        path: '/',
        component: LandingPage,
        private: false,
        exact: true,
    }
];