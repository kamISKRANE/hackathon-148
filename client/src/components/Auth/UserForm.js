import React from "react";
import { connect } from 'react-redux';
import { loginUser } from './actions';
import Typography from '../../ui/Typography/Typography';
import Card from '../../components/Card/Card';
import Input from '../../ui/Inputs/Input';
import {Container, Row, Col} from 'react-bootstrap';
import Button from '../../ui/Buttons/Button';
import logoWhite from '../../public/img/logo-white.svg';
import { colors } from "../../colors";

const styles = {
  brand: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    width: '75px',
    height: '75px',
    marginBottom: '2rem',
  },
};
class UserForm extends React.Component {
  state = {};

  handleSubmit(e) {
    // prevent submit
    e.preventDefault();
    // loginUser
    this.props.loginUser(this.state);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  togglePasswordVisibility() {
    this.setState({
      isPasswordVisible: !this.state.isPasswordVisible,
    });
  }

  render() {
    const { is_logging } = this.props;
    return (
      <React.Fragment>
          <Container>      
              <Row className={'justify-content-center'}>
                  <Col lg={4} md={8}>
                      <Card style={{backgroundColor: colors.primary.normal, color: 'white'}}>
                          <div style={styles.brand}>
                            <img src={logoWhite} style={styles.img} />
                          </div>
                          <form
                              onSubmit={(e) => this.handleSubmit(e)}
                          >
                            <Typography variant='title_1' style={{textAlign: 'center', color: 'white'}}>
                                Administrez vos salons ici
                            </Typography>
                            <Typography variant='body' style={{color: 'white'}}>
                                Email
                            </Typography>
                            <Input 
                                type='email'
                                fullWidth
                                name='email'  
                                onChange={(e) => this.onChange(e)}
                                required
                            />
                            <Typography variant='body' style={{color: 'white'}}>
                                Mot de passe
                            </Typography>
                            <Input 
                                type='password' 
                                name='password'  
                                fullWidth
                                onChange={(e) => this.onChange(e)}
                                required
                                />
                            <Button
                                fullWidth
                                type='submit'
                                variant='outlined'
                                content={is_logging ? 'Connexion en cours...' : 'Se connecter'}
                                disable={is_logging}
                                style={{borderColor: 'white', color: 'white'}}
                            />
                          </form>
                      </Card>
                  </Col>
              </Row>
          </Container>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  is_logging: state.auth.is_logging
});

export default connect(mapStateToProps, { loginUser })(UserForm);
