import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from './actions'
import history from '../../configs/history';
import { auth } from './api';

export function* register(action) {
  try {
    const response = yield call(auth.register, action.payload);
    yield put({type: actions.REGISTER_SUCCESS, payload: response});
    history.replace('/login');
  } catch (e) {
    yield put({type: actions.REGISTER_FAILED});
    console.log(e)
  }
}

export function* login(action) {
  try {
    const response = yield call(auth.login, action.payload);
    yield put({type: actions.LOGIN_SUCCESS, payload: response});
    history.replace('/dashboard');
  } catch (e) {
    yield put({type: actions.LOGIN_FAILED});
    console.log(e)
  }
}

function* authSaga() {
  yield takeLatest(actions.REGISTER, register);
  yield takeLatest(actions.LOGIN, login);
}

export default authSaga;
