import * as actions from './actions';

const initialState = {
  fetching: false,
  confirmed: false,
  user: null,
  token: null,
  is_logging: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case actions.LOGIN:
      return {
        is_logging: true,
      };
      break;
    case actions.LOGIN_USER:
      return {
        is_logging: true,
      };
      break;
    case actions.LOGIN_SUCCESS:
      return {
        user: action.payload,
        token: action.payload.token,
        is_logging: false,
        confirmed: true,
      };
      break;
    case actions.LOGIN_FAILED:
      return {
        is_logging: false,
      };
      break;
    case actions.LOGOUT_USER:
      return {
        ...state,
        is_logging: true,
      };
      break;
    case actions.LOGOUT_SUCCESS:
      return {
        user: null,
        token: null,
        is_logging: false,
        confirmed: false,
      };
      break;
    case actions.LOGOUT_FAILED:
      return {
        ...state,
        is_logging: false,
      };
      break;
    case actions.CONFIRM:
      return {
        ...state,
        fetching: true,
      };
    case actions.CONFIRM_SUCCESS:
      return {
        ...state,
        fetching: false,
        confirmed: true,
      };
    case actions.CONFIRM_FAILED:
      return {
        fetching: false,
        confirmed: false,
      };
    case actions.REGISTER:
      return {
        ...state,
        fetching: true,
      };
    case actions.REGISTER_SUCCESS:
      return {
        ...state,
        fetching: false,
      };
    case actions.REGISTER_FAILED:
      return {
        fetching: false,
      };
    case actions.SET_TOKEN:
      return {
        ...state,
        token: action.payload,
      };
    default:
      return state;
  }
}
