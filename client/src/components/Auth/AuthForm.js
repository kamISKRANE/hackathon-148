import React from "react";
import {isMobile} from "../../utils/utils";
import { connect } from 'react-redux';
import { login } from './actions';
import './style.css';

const styles =  {
  fullHeight: {
    minHeight: '100vh',
    height: '100vh',
  },
  container: {
    height: '100%',
    paddingTop: '56px',
  },
};

class AuthForm extends React.Component {
  state = {
    cpt: 0,
  };

  handleChange = (e) => this.setState({
    [e.target.name]: e.target.value,
  });

  onSubmit = (e) => {
    e.preventDefault();
    this.props.login(this.state);
  };

  render() {
    const { user } = this.props;
    return (
      <div style={styles.fullHeight}>
        Formulaire d'authentification AGENT DU WEB
        <form onSubmit={(e) => this.onSubmit(e)}>
          <input type='text' name='email'  placeholder='Username or email' onChange={(e) => this.handleChange(e)} />
          <input type='password' name='password' placeholder='Password' onChange={(e) => this.handleChange(e)} />
          <button type='submit'>Sign in</button>
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.auth && state.auth.user,
});

export default connect(mapStateToProps, { login })(AuthForm);