export const LOGIN = 'LOGIN';
export const LOGIN_USER = 'LOGIN_USER';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGOUT_USER = 'LOGOUT_USER';
export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_FAILED = 'LOGOUT_FAILED';
export const CONFIRM = 'CONFIRM';
export const CONFIRM_SUCCESS = 'CONFIRM_SUCCESS';
export const CONFIRM_FAILED = 'CONFIRM_FAILED';
export const REGISTER = 'REGISTER';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_FAILED = 'REGISTER_FAILED';
export const SET_TOKEN = 'SET_TOKEN';

export const login = (credentials) => ({
  type: LOGIN,
  payload: credentials
})

export const loginUser = (credentials) => ({
  type: LOGIN_USER,
  payload: credentials
})

export const logoutUser = () => ({
  type: LOGOUT_USER
})

export const confirm = (token) => ({
  type: CONFIRM,
  payload: token
})
export const register = (user) => ({
  type: REGISTER,
  payload: user,
})

export const setToken = (token) => ({
  type: SET_TOKEN, payload: token,
})