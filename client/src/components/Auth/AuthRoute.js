import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { confirm, setToken } from './actions';
import qs from 'qs';

class AuthRoute extends React.Component {
  componentDidMount() {
    let { token, confirmed } = this.props;
    if (this.props.location.search)
    {
      token = qs.parse(this.props.location.search.substr(1)).token;
      this.props.setToken(token);
    }
    if (token && !confirmed) {
      this.props.confirm(token);
    }
  }

  render() {
    const { confirmed, fetching, token } = this.props;

    if (fetching) return 'Loading ...';

    return (
      <React.Fragment>
        {
          (!token && !confirmed && !fetching) && <Redirect to='/login' />
        }
        {
          (!fetching && confirmed) && <Route {...this.props} />
        }
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  confirmed: state.auth.confirmed,
  token: state.auth.token,
  fetching: state.auth.fetching,
  user: state.auth.user,
});

export default connect(mapStateToProps, { confirm, setToken })(AuthRoute);
