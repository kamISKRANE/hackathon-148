import React from 'react';
import { connect } from 'react-redux';
import { register } from './actions';

class RegisterForm extends React.Component {
    state = {};

    submit = (e) => {
        e.preventDefault();
        this.props.register({ ...this.state, role: '2' });
    }

    onChange = (e) => this.setState({ [e.target.name]: e.target.value });

    render() { 
        return (
            <div>
                <form onSubmit={(e) => this.submit(e)} className={'d-flex flex-column'}>
                    Devenez un agent.
                    <input onChange={(e) => this.onChange(e)} type='text' name='prenom' placeholder='Firstname' />
                    <input onChange={(e) => this.onChange(e)} type='text' name='nom' placeholder='Lastname' />
                    <input onChange={(e) => this.onChange(e)} type='text' name='email' placeholder='Username or email' />
                    <input onChange={(e) => this.onChange(e)} type='password' name='password' placeholder='Password' />
                    <input onChange={(e) => this.onChange(e)} type='password' name='confirm_password' placeholder='Repeat password' />
                    <button type='submit'>Submit</button>
                </form>
            </div>
        );
    }
}
 
export default connect(null, { register })(RegisterForm);