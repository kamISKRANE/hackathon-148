import { confs } from '../../configs/conf';
import Request from "../../utils/Request";

const { URL_API } = confs;

let request = new Request();

export const auth = {
  login: (credentials) => request.post('/login_check', credentials)
      .then(data => data.data),
  register: (agent) => request.post('/user_accounts', agent)
      .then(data => data.data),
};
