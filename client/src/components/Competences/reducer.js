import * as actions from './actions';

const initialState = {
    competences: null,
    is_fetching: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case actions.FETCH_CORPS_DE_METIER:
      return {
            ...state,
            is_fetching: true,
      };
      break;
    case actions.FETCH_CORPS_DE_METIER_SUCCESS:
      return {
            ...state,
            corps_de_metiers: action.payload,
            is_fetching: false,
      };
      break;
    case actions.FETCH_CORPS_DE_METIER_FAILED:
      return {
          ...state,
          is_fetching: false,
      };
      break;
    default:
      return state;
      break;
  }
}
