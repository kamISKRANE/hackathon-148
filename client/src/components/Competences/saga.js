import { call, put, takeLatest } from 'redux-saga/effects';
import * as actions from './actions'
import { competences } from './api';

export function* fetchCorpsDeMetiers(action) {
  try {
    const response = yield call(competences.fetchCorpsDeMetiers, action.payload);
    yield put({type: actions.FETCH_CORPS_DE_METIER_SUCCESS, payload: response});
  } catch (e) {
    yield put({type: actions.FETCH_CORPS_DE_METIER_FAILED});
    console.log(e)
  }
}

function* competencesSaga() {
  yield takeLatest(actions.FETCH_CORPS_DE_METIER, fetchCorpsDeMetiers);
}

export default competencesSaga;
