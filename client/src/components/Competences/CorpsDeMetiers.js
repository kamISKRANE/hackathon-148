import React from 'react';
import { connect } from 'react-redux';
import { fetchCorpsDeMetiers } from './actions';
import AdminMenu from '../Menus/AdminMenu';
import { Link } from 'react-router-dom';

class CorpsDeMetiers extends React.Component {
    state = {};

    componentDidMount() {
        this.props.fetchCorpsDeMetiers();
    }

    render() { 
        const { is_fetching, corps_de_metiers } = this.props;

        

        return (
            <div>
                <AdminMenu />
                <div className={'container'}>
                    <h1>Corps de métier</h1>
                    { is_fetching && 'Loading ...'}
                    <div className={'row'}>
                            {
                                corps_de_metiers && corps_de_metiers['hydra:member'].map((cm, key) => (
                                    <div className={'col-md-4'}>
                                        <div className="card">
                                            <div className="card-body">
                                                <h5 className="card-title">{cm.libelle}</h5>
                                                <h6 className="card-subtitle mb-2 text-muted">Contient {cm.domaineDeCompetences.length} domaines de compétences.</h6>
                                                <Link to={'/admin/competences/'+cm.id} className="card-link">Voir plus</Link>
                                            </div>
                                        </div>
                                    </div>
                                ))
                            }
                        </div>
                </div>
            </div>
        );
    }
}
 
const mapStateToProps = state => ({
    corps_de_metiers: state.competences.corps_de_metiers,
    is_fetching: state.competences.is_fetching,
});

export default connect(mapStateToProps, { fetchCorpsDeMetiers })(CorpsDeMetiers);