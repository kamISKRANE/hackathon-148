import Request from '../../utils/Request';

let req = new Request();

export const competences = {
    fetchCorpsDeMetiers: () => req.get('/corps_de_metiers').then(data => data.data),
    fetchDomainesDeCompetences: () => req.get('/domaine_de_competences').then(data => data.data),
};