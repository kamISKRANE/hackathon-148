import { call, put, takeLatest } from 'redux-saga/effects';
import { greetings } from './api';
import * as actions from './actions';

export function* fetchGreetings (action) {
     try {
        const response = yield call(greetings.fetchGreetings);
        yield put({ type: actions.FETCH_GREETINGS_SUCCESS, payload: response});
    } catch (error) {
        yield put({ type: actions.FETCH_GREETINGS_FAILED});
        console.log(error);
     }
} 

function* greetingsSaga() {
    yield takeLatest(actions.FETCH_GREETINGS, fetchGreetings);
}

export 
default greetingsSaga;