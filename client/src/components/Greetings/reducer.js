import * as actions from './actions';

const initialState = {
    greetings: null,
    is_fetching: false,
};

export default function(state=initialState, action) {
    switch (action.type) {
        case actions.FETCH_GREETINGS: 
            return {
                ...state,
                is_fetching: true,
            };
            break;
        case actions.FETCH_GREETINGS_SUCCESS:
            return {
                ...state,
                greetings: action.payload.data,
                is_fetching: false,
            };
            break;
        case actions.FETCH_GREETINGS_FAILED: 
            return {
                ...state,
                is_fetching: false,
            };
            break;
        default: 
            return state;
    }
}