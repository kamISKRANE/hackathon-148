export const FETCH_GREETINGS = 'FETCH_GREETINGS';
export const FETCH_GREETINGS_SUCCESS = 'FETCH_GREETINGS_SUCCESS';
export const FETCH_GREETINGS_FAILED = 'FETCH_GREETINGS_FAILED';
export const fetchGreetings = () => ({
    type: FETCH_GREETINGS, 
});

export const POST_GREETING = 'POST_GREETING';
export const POST_GREETING_SUCCESS = 'POST_GREETING_SUCCESS';
export const POST_GREETING_FAILED = 'POST_GREETING_FAILED';
export const postGreeting = () => ({
    type: POST_GREETING, 
});