import Request from '../../utils/Request';

let req = new Request();

export const greetings = {
    fetchGreetings: () => req.get('/greetings')
        .then(data => data),
}