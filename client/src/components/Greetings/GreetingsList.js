import React from 'react';
import { connect } from 'react-redux';
import { fetchGreetings } from './actions'

class Greeting extends React.Component {
    state = {  }
    componentDidMount() {
        this.props.fetchGreetings();
    }

    render() { 
        const { greetings, is_fetching } = this.props;

        if (is_fetching) return 'Loading greetings...';

        return ( 
            <div>
                List of greetings 
                {
                    greetings && greetings['hydra:member'].map((greet, key) => (
                        <div key={key}>
                            {greet.name}
                        </div>
                    ))
                }
            </div>
         );
    }
}
 
const mapStateToProps = state => ({
    greetings: state.greetings.greetings,
    is_fetching: state.greetings.is_fetching
});

export default connect(mapStateToProps, { fetchGreetings })(Greeting);