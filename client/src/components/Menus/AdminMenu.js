import React from 'react';
import { Link } from 'react-router-dom';

class AdminMenu extends React.Component {
    state = {  }
    render() { 
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-light mb-2">
                <a className="navbar-brand" href="#">ADW</a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item active">
                            <Link className="nav-link" to={'/admin/dashboard'}>Dashboard</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={'/admin/database-knowledge'}>Database Knowledge</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={'/admin/events'}>Events</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={'/admin/candidates'}>Candidates</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" to={'/admin/competences'}>Gestion des compétences</Link>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}
 
export default AdminMenu;