import React from 'react';
import { Link } from 'react-router-dom';

class AgentMenu extends React.Component {
    state = {  }
    render() { 
        return (
            <div>
                <Link to={'/dashboard'}>Dashboard</Link>
                <Link to={'/database-knowledge'}>Database knowledge</Link>
                <Link to={'/events'}>Events</Link>
                <Link to={'/candidates'}>Candidatures</Link>
            </div>
        );
    }
}
 
export default AgentMenu;