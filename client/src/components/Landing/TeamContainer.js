import React, { Component } from 'react';

const style = {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    height: '400px',
}

class TeamContainer extends Component {
    render() { 

        const { children } = this.props ;

        return (
            <div style={style} id="team-container">
                { children }
            </div>
        );
    }
}
 
export default TeamContainer;