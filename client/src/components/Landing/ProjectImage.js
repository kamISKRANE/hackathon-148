import React, { Component } from 'react';

class ProjectImage extends Component {
    render() {

        const { image } = this.props

        const style = {
            container: {
                width: '50%',
                display: 'flex',
                flexWrap: 'wrap',
                justifyContent: 'center',
                alignItems: 'center',
        
            },
            image: {
                height: '300px',
                width: '100%',
                backgroundImage: `url(${image})`, 
                backgroundSize: 'contain', 
                backgroundPosition: 'center', 
                backgroundRepeat: 'no-repeat',
            }
        };

        return (
            <div style={style.container}>   
                <div style={style.image}>

                </div>
            </div>
        );
    }
}
 
export default ProjectImage;