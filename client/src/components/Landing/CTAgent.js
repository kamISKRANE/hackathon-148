import React, { Component } from 'react';
import CTAgentImage from '../../public/img/btn_valider.svg'

const style = {
    container: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
    agent: {
        width: '150px',
        height: '150px',
        backgroundImage: `url(${CTAgentImage})`, 
        backgroundSize: 'contain', 
        backgroundPosition: 'center', 
        backgroundRepeat: 'no-repeat',
        margin: '10rem 0',
    }
}

class CTAgent extends Component {
    render() { 
        return (
            <div style={style.container}>
                <div style={style.agent}></div>
            </div>
        );
    }
}
 
export default CTAgent;