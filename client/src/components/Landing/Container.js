import React, { Component } from 'react';
import wallpaper from '../../public/img/begin_fond.svg'

const style = {
    container: {
        width: '100%', 
        minHeight: '100vh', 
        backgroundImage: `url(${wallpaper})`, 
        backgroundSize: 'contain', 
        backgroundPosition: 'center', 
        backgroundRepeat: 'no-repeat',
    },
}


class Container extends Component {
    render() { 
        return (
            <div style={style.container}>
                { this.props.children }
            </div>
        );
    }
}
 
export default Container;