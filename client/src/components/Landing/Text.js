import React, { Component } from 'react';

const style = {
    firstText: {
        fontSize: "20px",
        margin: '4rem 0 0',
        padding: ' 2rem',
        wordSpacing: '0.5rem',
    },
    other: {
        fontSize: "20px",
        margin: '1rem 0',
        padding: '0 2rem',
        wordSpacing: '0.5rem',
    },
    lastTest: {
        fontSize: "20px",
        margin: '1rem 0',
        padding: '0 2rem',
        wordSpacing: '0.5rem',
        zIndex: 6,
    },
}

class Text extends Component {

    render() { 

        const {content, first, children, last } = this.props;
        const { firstText, other, lastTest } = style

        return (
            <p style={ first ? firstText : last ? lastTest : other }>
                { children }
            </p>
        );
    }
}
 
export default Text;