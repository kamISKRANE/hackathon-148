import React, { Component } from 'react';
import AsideLogos from './AsideLogos';
import Texts from './Texts';
import fond from '../../public/img/6poles.svg'
import Projects from './Projects';
import Teams from './Teams';

const style = {
    container: {
        background: 'white',
        width: '100%',
        height: '1400px',
        backgroundImage: `url(${fond})`, 
        backgroundSize: 'contain ', 
        backgroundPosition: 'center', 
        backgroundRepeat: 'no-repeat',
        display: 'flex',
        position: 'relative',
        zIndex: 4,
        flexWrap: 'wrap',
        
    },
}

class Explanation extends Component {
    render() { 
        return ( 
            <div style={style.container}>

                <AsideLogos />
                <Texts />

                <Projects />

                <Teams />
               
            </div>
        );
    }
}
 
export default Explanation;