import React, { Component } from 'react';
import ProjectContainer from './ProjectContainer';
import ProjectImage from './ProjectImage';
import alliance_nature from  '../../public/img/alliance_nature.svg'
import Header from './Header';
import TeamContainer from './TeamContainer';
import TeamImage from './TeamImage';
import leSage from '../../public/img/le-sage.svg';
import leGood from '../../public/img/le-good-vibe.svg';
import leVegan from '../../public/img/la-vegan.svg';
import leCrea from '../../public/img/le-crea.svg';

const style = {
    container: {
        display : 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: '100%',
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
    }

};


class ProjectContainers extends Component {
    render() { 

        const { children } = this.props;
        return (
            <div style={style.container} id="teams">

                <TeamContainer>
                    <TeamImage image={leSage}/>
                    <TeamImage image={leGood}/>
                </TeamContainer>

                <TeamContainer>
                    <TeamImage image={leVegan}/>
                    <TeamImage image={leVegan}/>
                </TeamContainer>
            </div>
        );
    }
}
 
export default ProjectContainers;