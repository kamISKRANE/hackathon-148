import React, { Component } from 'react';
import VerticalShape from './VerticalShape';
import LetterSpacedTitle from './LetterSpacedTitle';
import CTAgent from './CTAgent';
import ProjectContainers from './ProjectContainers';


const style = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        position: 'relative',
        height: '1rem',
        width: '100%',
        height: 'auto',
        zIndex: 25
    },
}

class Projects extends Component {

    render() { 
        return (
            <div style={style.container} id='projects'>

                <LetterSpacedTitle left="Les" right="Projets"></LetterSpacedTitle>

                <CTAgent />

                <ProjectContainers />

            </div>
        );
    }
}
 
export default Projects;