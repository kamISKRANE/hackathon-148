import React, { Component } from 'react';
import logo from '../../public/img/148.svg'

class LogoADW extends Component {
    render() { 

        const { width, height, margin } = this.props;

        return ( 
            <img src={logo} style={{
                width: `${width}px`,
                height: `${height}px`,
                margin: margin,
            }}/>
        );
    }
}
 
export default LogoADW;