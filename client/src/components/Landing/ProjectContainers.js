import React, { Component } from 'react';
import ProjectContainer from './ProjectContainer';
import ProjectImage from './ProjectImage';
import Header from './Header';
import image_1 from  '../../public/img/image_1.svg'


const style = {
    container: {
        display : 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        width: '100%',
        height: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
    }

};


class ProjectContainers extends Component {
    render() { 
        return (
            <div style={style.container}>

                <ProjectContainer>
                    <ProjectImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                    <ProjectImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                </ProjectContainer>

                <ProjectContainer>
                    <ProjectImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                    <ProjectImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                </ProjectContainer>

                <ProjectContainer>
                    <ProjectImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                    <ProjectImage/>
                </ProjectContainer>
                
            </div>
        );
    }
}
 
export default ProjectContainers;