import React, { Component } from 'react';
import { ReactComponent as Logo148 }     from '../../public/img/148.svg';


const style = {
    container: {
        width: '100%',
        display: 'flex',
        alignItems: 'center',
    },
    image: {
        width: '50px',
        heigth: '50px',
    },
    children: {
        margin: '3rem',
        display: 'flex',
        flex: '1',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '100%',
    },
    icon: {
        margin: '1rem',
    },
    fr: {
        padding: '0.5rem',
        fontWeight : 'bold'
    },
    en: {
        padding: '0.5rem',

    }
};


class Header extends Component {
    render() { 
        return ( 
            <div style={style.container}>
                
                <Logo148 heigth='100' width='100' margin='0 2rem' type={Logo148}/>
                
                <div style={style.children}>
                    <span style={style.en}>En</span>
                    <span>/</span>
                    <span style={style.fr}>Fr</span>
                    <i className="fa fa-2x fa-bars" style={style.icon}></i>
                </div>

            </div>
         );
    }
}
 
export default Header;
