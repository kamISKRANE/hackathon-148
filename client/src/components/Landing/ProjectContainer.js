import React, { Component } from 'react';

const style = {
    container: {
        display: 'flex',
        justifyContent: 'space-between',
        width: '100%',
        height: 'auto',
        margin: '0 0 3rem',
        flexWrap: 'wrap',
    },
} 

class ProjectContainer extends Component {
    render() { 

        const { children } = this.props;

        return (
            <div style={style.container}>
                { children }
            </div>
        );
    }
}
 
export default ProjectContainer;