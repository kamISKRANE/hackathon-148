import React, { Component } from 'react';
import LetterSpacedTitle from './LetterSpacedTitle'
import TeamsContainers from './TeamsContainers'
import TeamContainer from './TeamContainer'
import TeamImage from './TeamImage'

const style = {
    container: {
        display: 'flex',
        flexWrap: 'wrap',
        position: 'relative',
        height: '1rem',
        width: '100%',
        height: 'auto',
        zIndex: 25
    },
}

class Teams extends Component {
    render() { 
        return (
            <div style={style.container} id='projects'>

                <LetterSpacedTitle left="Les" right="équipes"></LetterSpacedTitle>

                <TeamsContainers>

                    <TeamContainer>
                        <TeamImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                        <TeamImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                    </TeamContainer>

                    <TeamContainer>
                        <TeamImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                        <TeamImage image={'https://www.bigstockphoto.com/images/homepage/module-6.jpg'} />
                    </TeamContainer>

                </TeamsContainers>
            </div>
        );
    }
}
 
export default Teams;