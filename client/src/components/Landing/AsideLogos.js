import React, { Component } from 'react';
import Logo from './Logo';
import LogoADW from '../../public/img/logoADW.svg'

const style = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        width: 'auto',
        flex: 1,
    },
    logo: {
        width: '50px',
        heigth: '50px',
    },
}

class AsideLogos extends Component {
    
    render() { 
        return (
            <div style={style.container}>
                <Logo heigth='300' width='300' type={LogoADW} margin='10rem 2rem'/>

            </div>
        );
    }
}
 
export default AsideLogos;