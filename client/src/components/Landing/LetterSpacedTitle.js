import React, { Component } from 'react';

const style = {
    rightS: {
        letterSpacing: '0.5rem',
        textTransform: 'uppercase',
        fontSize: '72px',
        letterSpacing: '2rem',
        margin: '0 5rem 0 0',
        fontSize: '98px',
        width: '50%',
        textAlign: 'right',
        
    },
    leftS: {
        fontWeight: 'bold',
        textTransform: 'uppercase',
        margin: '0 5rem 0 0',
        letterSpacing: '2rem',
        fontSize: '98px',
        width: '50%',
        textAlign: 'right',
    },
    container: {
        display: 'flex',
        justifyContent: 'flex-end',
        width: '100%',
        zIndex: 25
    }
}

class LetterSpacedTitle extends Component {

    render() { 

        const { left, right } = this.props;
        const { container, leftS, rightS } = style;

        return (
            <div style={container}>
                <h1 style={leftS}>{left}</h1>
                <h1 style={rightS}>{right}</h1>
            </div>

        );
    }
}
 
export default LetterSpacedTitle;