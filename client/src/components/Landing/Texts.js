import React, { Component } from 'react';
import Logo148 from '../../public/img/148.svg'
import Text from './Text';
import Logo from './Logo';

const style = {
    container: {
        display: 'flex',
        flexDirection: 'column',
        flex: 4,
        textAlign: 'justify',
    },
}

class Texts extends Component {


    render() { 
        return (
            <div style={style.container}>

                <Logo heigth='300' width='300' type={Logo148} margin='0 2rem'/>

                <Text style={style.firstText} first='true'>
                    en 2020 est inclusive en offrant l’opportunité  à tout agent du web de répondre à des projets
                    d’envergures !"
                </Text>
                <Text style={style.otherText}>
                    s’engage à proposer des projets à tout niveau 
                    avec des entreprises certifié B Corp. Ce label le gage que l’Agence 148 s’implique et cherche
                    constamment à s’améliorer auprès des agents du web.
                </Text>

                <Text style={style.otherText}>
                    Paragraphe sur la communauté
                </Text>

                <Text style={style.otherText} last='true'>
                    On s’implique pour mettre à votre disposition
                    une solution de transmition de connaissance
                    afin d’acquérir des compétences nécessaires
                    pour prétendre des futurs agents du web.
                </Text>
            </div>
        );
    }
}
 
export default Texts;