import React, { Component } from 'react';
import verticalShape from '../../public/img/motif_peinture_project.svg';

const style = {
    position: 'absolute',
    width: '600px',
    height: '900px',
    backgroundImage: `url(${verticalShape})`, 
    backgroundSize: 'contain', 
    backgroundPosition: 'center', 
    backgroundRepeat: 'no-repeat',
    top: '-40%',
    left: '2%',
    zIndex: 20
}

class VerticalShape extends Component {
    render() { 
        return (
            <div style={style}></div>
        );
    }
}
 
export default VerticalShape;