import React, { Component } from 'react';

class Logo extends Component {
    render() { 

        const { width, height, margin, type } = this.props;

        return ( 
            <img src={type} style={{
                width: `${width}px`,
                height: `${height}px`,
                margin: margin,
            }}/>
        );
    }
}
 
export default Logo;