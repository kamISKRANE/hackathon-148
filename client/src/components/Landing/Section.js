import React, { Component } from 'react';
import wallpaper from '../../public/img/wallpaper.svg'

const style = {
    backgroundImage: {
        width: '100%', 
        height: '100vh', 
        backgroundImage: `url(${wallpaper})`, 
        backgroundSize: 'contain', 
        backgroundPosition: 'center', 
        backgroundRepeat: 'no-repeat',
    }
}

class Section extends Component {

    render() {
        return (
            <div style={style.backgroundImage}></div>
        );
    }
}
 
export default Section;