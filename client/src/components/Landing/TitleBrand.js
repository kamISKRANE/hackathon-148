import React, { Component } from 'react';

const style = {
    titre: {
        position: 'absolute',
        top: '45%',
        left: '50%',
        transform: 'translate(-50%, -50%) rotate(-10deg)',
        textAlign: 'center',
        width: '100%',
        textTransform: 'uppercase',
        letterSpacing: '1.5rem',
        fontSize: '5rem',
    },
}

class TitleBrand extends Component {

    render() { 
        return ( 
            <h1 style={style.titre}>agent du web</h1>
        );
    }
}
 
export default TitleBrand;