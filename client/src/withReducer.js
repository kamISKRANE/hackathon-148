import React from 'react'
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

export default (mapState, mapDispatch, new_context = null) => WrappedComponent => {
    class WithReducer extends React.PureComponent {
        constructor (props) {
            super(props);
        }

        componentDidMount() {
            if (new_context) {
                console.log(this.context.store);
                // store.injectReducer(new_context.key, new_context.reducer);
            }
        }
    
        render() {
            return <WrappedComponent />;
        }
    };

    return connect(mapState, mapDispatch)(WithReducer);
};
