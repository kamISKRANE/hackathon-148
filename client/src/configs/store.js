import React from 'react'
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import thunk from 'redux-thunk';
import { loadState, saveState } from './localStorage';
import createReducer from '../reducers';
import createSagaMiddleware from 'redux-saga';
import { rootSagas } from '../sagas';

// Prepare sagas middleware
const sagaMiddleware = createSagaMiddleware();

export const initializeSagas = () => {
  sagaMiddleware.run(rootSagas);
};

/* Redux thunk middleware */
const middleware = [thunk];

export const initializeStore = () => {

  const initialState = loadState();

  const store = createStore(
    createReducer(),
    initialState,
    getMiddleWare()
  );

  store.asyncReducers = {};
  store.injectReducer = (key, reducer) => {
    console.log('store.injectReducer', { key, reducer });
    store.asyncReducers[key] = reducer;
    store.replaceReducer(createReducer(store.asyncReducers));
    return store;
  };

  store.subscribe(() => {
    saveState(store.getState());
  });

  return store;

};

function getMiddleWare() {
  if ((!process.env.NODE_ENV || process.env.NODE_ENV === 'development') /*&& !isMobile()*/) {
    return compose(
        applyMiddleware(
            ...middleware,
            sagaMiddleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      );
  }else {
      return applyMiddleware(...middleware, sagaMiddleware);
  }
}
