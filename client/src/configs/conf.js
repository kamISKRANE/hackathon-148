let DEVELOPMENT_MODE;
let EXECUTION_MODE;
let confs;

DEVELOPMENT_MODE = (!process.env.NODE_ENV || process.env.NODE_ENV === 'development');

function getHost() {
  if (DEVELOPMENT_MODE) {
    EXECUTION_MODE = "development";
  } else {
    EXECUTION_MODE = "production" // window.location.hostname;
  }
  return EXECUTION_MODE;
}

/*
  ** Configurations
*/
function getConfigurations (execution_mode) {
  switch (execution_mode) {
    case "development":
      confs = {
        URL_API: "https://localhost:8443",
        FAVICON: '/logo-white.svg',
        TITLE: 'ADW',
        META_NAME_CONTENT: 'rgb(255, 241, 4)',
        MANIFEST: '/manifest.json',
        PRIMARY_BG: 'rgb(255, 241, 4)',
      };
      break;
      //
    case "production":
      confs = {
        URL_API: "http://diapoflow.fr/api",
        FAVICON: '/logo-white.svg',
        TITLE: 'Diapoflow',
        META_NAME_CONTENT: 'rgb(255, 241, 4)',
        MANIFEST: '/manifest.json',
        PRIMARY_BG: 'rgb(255, 241, 4)',
        SITE_MARCHAND: 'https://diapoflow.fr/'
      };
      break;
    default:
      console.error('No configurations find for ' + EXECUTION_MODE + " mode.");
      break;

  }
}

/*
  ** Setting attributes for index.html
*/
export function applyConfiguration() {
  /*
    ** Favicon
  */
  document.getElementById('FAVICON').setAttribute('href', confs.FAVICON);
  /*
    ** Manifest
  */
  document.getElementById('MANIFEST').setAttribute('href', confs.MANIFEST);
  /*
    ** Manifest
  */
  document.getElementById('TITLE').innerHTML = confs.TITLE;
  /*
    ** Meta Themecolor
  */
  document.getElementById('META_NAME_CONTENT').setAttribute('content', confs.META_NAME_CONTENT);

}

getConfigurations(getHost());

applyConfiguration();

export { confs };

