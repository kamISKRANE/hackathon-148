import { createMuiTheme } from '@material-ui/core';

export function buildTheme(type) {
  return createMuiTheme({
    palette: {
      primary: {
        main: 'rgb(1, 8, 43)',
        // main: 'rgb(255, 241, 4)',
        dark: '#424242',
      },
      // secondary: {},
      // action: {},
      type: type,
    },
    typography: {
      fontFamily:  "'Quicksand', sans-serif",
      fontWeightRegular:  "500",
    }
  });
}