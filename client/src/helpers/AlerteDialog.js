import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Button from '../ui/Buttons/Button';
import { colors } from '../colors';
import './style.css'

class AlerteDialog extends React.Component {
    state = {
        open: false,
    };

    componentDidMount() {
        window.alert = (title, message, okCallback, okButtonText = 'Ok') => {
            this.setState({
                open: true,
                title,
                message,
                okCallback,
                okButtonText,
            });
        }

        window.alerteComponent = ({
            title,
            message,
            render,
            okCallback,
            okButtonText = 'Ok'
        }) => this.setState({
            open: true,
            title,
            message,
            render,
            okCallback,
            okButtonText,
        });
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    ok = () => {
        this.state.okCallback();
        this.handleClose();
    }

    render() {
        const { open, message, render, title, okButtonText } = this.state;
        
        return (
            <Dialog
                open={open}
                onClose={() => this.handleClose()}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle style={{backgroundColor: colors.primary.normal, color: 'white'}} id="alert-dialog-title">
                    {title}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {render ? render() : message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.ok()} variant="secondary">
                        {okButtonText}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }   
}

export default AlerteDialog;