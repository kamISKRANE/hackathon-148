import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Button from '../ui/Buttons/Button';
import { colors } from '../colors';

class ConfirmDialog extends React.Component {
    state = {
        open: false,
    };

    componentDidMount() {
        window.confirm = (title, message, yesCallback, noCallback, yesButtonText = 'Ok', noButtonText = 'Cancel') => {
            this.setState({
                open: true,
                title,
                message,
                yesCallback,
                noCallback,
                yesButtonText,
                noButtonText,
            });
        }
    }

    handleClose = () => {
        this.setState({ open: false });
        // window.showToast({
        //     message: 'My first toast',
        //     actions: [
        //         {label: 'Click me', onClick: () => alert('Oooh! you clicked me!')}
        //     ],
        //     delay: 6000,
        // });
    };

    yes = () => {
        this.state.yesCallback();
        this.handleClose();
    }

    no = () => {
        this.state.noCallback();
        this.handleClose();
    }

    render() {
        const { open, message, title, yesButtonText, noButtonText } = this.state;
        return (
            <Dialog
                open={open}
                onClose={() => this.handleClose()}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle style={{backgroundColor: colors.primary.normal, color: 'white'}} id="alert-dialog-title">{title}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => this.no()} variant="secondary">
                        {noButtonText}
                    </Button>
                    <Button onClick={() => this.yes()} variant="secondary" autoFocus>
                        {yesButtonText}
                    </Button>
                </DialogActions>
            </Dialog>
        );
    }   
}

export default ConfirmDialog;