import React from 'react';
import { Snackbar, IconButton } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import Button from '../ui/Buttons/Button';
import { colors } from '../colors';

const createStyles = (state) => {
    if (state.type === 'success') {
        return {
            backgroundColor: 'forestGreen',
            color: 'white',
        };
    }
}

class ConfirmDialog extends React.Component {
    state = {
        open: false,
    };

    componentDidMount() {
        window.showToast = ({ message, actions = [], closeButton = false, delay = 3000 }) => {
            this.setState({
                open: true,
                message,
                actions,
                closeButton,
                delay,
            }, () => (delay > 0) && window.setTimeout(() => (
                this.setState({ open: false })
            ), delay)); 
        }
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    handleClose = () => this.setState({ open: false });

    handleClickAction = (onClick) => {
        if (onClick) onClick();

        this.handleClose();
    }

    render() {
        const { open, message, actions, closeButton, delay } = this.state;
        return (
            <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={open}
                    message={message}
                    action={
                        <React.Fragment>
                            {
                                actions && actions.map(({label, ...others}, key) => (
                                    <Button key={key} variant="outlined" size="small" {...others} onClick={() => this.handleClickAction(others.onClick)}>
                                        {label}
                                    </Button>
                                ))
                            }
                            {
                                closeButton && 
                                    <IconButton size="small" aria-label="close" color="inherit" onClick={() => this.handleClose()}>
                                        <CloseIcon fontSize="small" />
                                    </IconButton>
                            }
                        </React.Fragment>
                        }
                />
            </div>
        );
    }   
}

export default ConfirmDialog;