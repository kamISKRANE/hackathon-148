import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogContentText, DialogActions } from '@material-ui/core';
import Button from '../ui/Buttons/Button';
import { colors } from '../colors';
import './style.css'

class DiapoflowDialog extends React.Component {
    state = {
        open: false,
    };

    componentDidMount() {
        window.openModal = ({
            title,
            render,
            persistant
        }) => this.setState({
            open: true,
            title,
            render,
            persistant,
        });

        window.closeModal = () => this.setState({ open: false });
    }

    handleClose = () => {
        this.setState({ open: false });
    };

    render() {
        const { open, render, title, persistant } = this.state;
        
        return (
            <Dialog
                open={open}
                onClose={() => persistant === true ? null : this.handleClose()}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle style={{backgroundColor: colors.primary.normal, color: 'white'}} id="alert-dialog-title">
                    {title}
                </DialogTitle>
                
                {render ? render() : null}

            </Dialog>
        );
    }   
}

export default DiapoflowDialog;