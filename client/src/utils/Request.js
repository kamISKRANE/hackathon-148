import axios from 'axios';
import qs from 'qs';
import { confs } from '../configs/conf';
import { loadState } from '../configs/localStorage';
// Retrieve api Domaine address
const { URL_API } = confs;
// Retrieve current state
//log
class Request  {
  // get method
  get(endpoint, params = {}, headers = {}) {
    return axios.get(URL_API + endpoint, this.getGetParams(params), {headers});
  }
  // post method
  post(endpoint, params = {}, headers = {}) {
    return axios.post(URL_API + endpoint, JSON.stringify(this.getPostParams(params)), this.getHeaders(headers));
  }
  // put method
  put(endpoint, params = {}, headers = {}) {
    return axios.put(URL_API + endpoint, JSON.stringify(this.getPostParams(params)), this.getHeaders(headers));
  }
  // delete method
  delete(endpoint, params = {}, headers = {}) {
    console.log(this.getHeaders(headers));
    return axios.delete(URL_API + endpoint, this.getPostParams(params), this.getHeaders(headers));
  }

  getDefaultParameters () {
    let state = loadState();
    return {};
  }

  getDefaultHeaders() {
    let state = loadState();
    const defaultHeaders = [];
    if (state && state.auth && state.auth.token) defaultHeaders['Authorization'] = state.auth.token;
    defaultHeaders['Content-type'] = 'application/json';
    return defaultHeaders;
  }
  // Adding headers
  getHeaders(headers) {
    let realHeaders = {
      headers: {
        ...headers,
        ...this.getDefaultHeaders(),
      }
    };
    return realHeaders;
  }
  // Adding params for request
  getGetParams(params) {
    let state = loadState();
    const realParams = {
      params: {
        ...params,
        ...this.getDefaultParameters(),
      }
    };
    if (state && state.auth && state.auth.token) realParams['params']['token'] = state.auth.token;

    return realParams;
  }

  getPostParams(params) {
    const realParams = {
      ...params,
      ...this.getDefaultParameters(),
    };

    return realParams;
  }

}

export default Request;
